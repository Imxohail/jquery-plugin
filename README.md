# README #

This is a simple Jquery plugin to perform certain array operations.

### Set up ###

* Include latest version of Jquery library.

* Sample tests are commented in the end of file.


### Contribution guidelines ###

* You can further add the array operations.