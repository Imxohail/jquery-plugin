(function($) {

  $.fn.getSet = function(arr, op, token) {
    var result = new Array();
    switch (op) {
      case "ucfirst": //Set first alphabet capital
        $.each(arr, function(index, value) {
          result.push(value.charAt(0).toUpperCase() + value.slice(1));
        });
        break;

      case "capitalize": //Set all alphabets capital
        $.each(arr, function(index, value) {
          result.push(value.toUpperCase());
        });
        break;

      case "lower": //Set all alphabets to lower case
        $.each(arr, function(index, value) {
          result.push(value.toLowerCase());
        });
        break;

      case "search": //Search the word in array
        if ((index = $.inArray(token, arr)) >= 0)
          result.push("index: " + index);
        else
          result.push("Not Found");
        break;

      case "sort": //Sort words
        result = arr.sort();
        break;

      case "string": //make a string
        result = arr.toString();
        break;

      case "reverse": //reverse order of elements
        result = arr.reverse();
        break;
    }
    return result;
  };

}(jQuery));

temp = ["one", "two", "three"];

//res = $().getSet(temp, 'ucfirst');
//res = $().getSet(temp, 'capitalize');
//res = $().getSet(temp, 'lower');
//res = $().getSet(temp, 'search', 'two');
//res = $().getSet(temp, 'sort');
//res = $().getSet(temp, 'string');
//res = $().getSet(temp, 'reverse');
$("<div id=\"res\"></div>").html(res);
